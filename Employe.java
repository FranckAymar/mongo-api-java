// Set MYPATH=D:\tpmongodb2021
// javac -g -cp %MYPATH%\mongojar\mongo-java-driver-3.12.7.jar;%MYPATH% %MYPATH%\tp\Employe.java
// java -Xmx256m -Xms256m -cp %MYPATH%\mongojar\mongo-java-driver-3.12.7.jar;%MYPATH% tp.Employe

/**
Le package TP contient deux classes. La classe Employe et la classe DeptEmp
Chacune de ces classes est dans son propre fichier.
La classe Employe contient les m�thodes suivantes :



public void insertOneEmploye(String nomCollection, Document employe);
public void testInsertOneEmploye();
public void insertManyEmployes(String nomCollection, List<Document> employes);
public void testInsertManyEmployes();
public void getEmployeById(String nomCollection, Integer empId);
public void getEmployes(String nomCollection, 
	Document whereQuery, 
	Document projectionFields,
	Document sortFields);
public void updateEmployes(String nomCollection, 
	Document whereQuery, 
	Document updateExpressions, UpdateOptions updateOptions);
public void deleteEmployes(String nomCollection, Document filters);
public void displayIterator(Iterator it, String message);   
public void joinLocalAndforeignCollections(
	String localCollectionName, 
	String ForeignCollectionName, 
	String localColJoinFieldName,
	String foreigColJoinFieldName,
	String filterFieldName,
	String filterFieldType,
	String filterFieldValue);
public void groupByOnOneCollection(String localCollectionName);
public void createEmployeIndexes(
		String localCollectionName,
		String indexName,
		String indexFieldName1,
		String indexFieldName2,
		String indexFieldName3,
		String indexType,
		boolean isAscendingIndex,
		boolean indexUnique);		
public void getAllIndexesOfACollection(String localCollectionName);
public void dropAIndexOfACollection(
		String localCollectionName,
		String indexName);

Chaque m�thode est d�crite lors de sa d�claration dans la classe.

*/

package tp;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase; 
import com.mongodb.MongoClient; 
import com.mongodb.MongoCredential; 
import com.mongodb.DBObject;  
import com.mongodb.BasicDBObject; 
import com.mongodb.DBCollection; 
import com.mongodb.DBCursor;
import com.mongodb.DB; 
import org.bson.Document;  
import java.util.Arrays;
import java.util.List;
import com.mongodb.client.FindIterable; 
import java.util.Iterator; 
import java.util.ArrayList;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.IndexOptions;

public class Employe { 
   private MongoDatabase database;
   private String dbName="RH";
   private String hostName="localhost";
   private int port=27017;
   private String userName="urh";
   private String passWord="passUrh";
   private String EmployeCollectionName="colEmployes";

   private MongoClient mongoClient;

   /**
   Le  
	
   */
   public static void main( String args[] ) {  
    try{
		Employe emp = new Employe();
		//emp.dropCollectionEmploye(emp.EmployeCollectionName);
		//emp.createCollectionEmploye(emp.EmployeCollectionName);
		//emp.deleteEmployes(emp.EmployeCollectionName, new Document());
		//emp.testInsertOneEmploye();
		//emp.testInsertManyEmployes();
		//emp.getEmployeById(emp.EmployeCollectionName, 7934);
		//emp.getAllEmployes(emp.EmployeCollectionName);
		// Afficher tous les employ�s sans tri ni projection
		//emp.getEmployes(emp.EmployeCollectionName, 
		//	new Document(), 
		//	new Document(),
		//	new Document());
		// Afficher tous les employ�s salesman du d�partement 30
		// Tri� en ordre croissant sur _id
		// Projet� sur _id, ename, job, deptno et adresse
		/*emp.getEmployes(emp.EmployeCollectionName, 
			new Document("job", "Salesman").append("deptno",30), 
			new Document("_id", 1).append("ename",1).
			append("job",1).append("deptno", 1).
			append("adresse",1),
			new Document("_id", 1).append("ename",1)
		);*/
		
		/*emp.updateEmployes(emp.EmployeCollectionName, 
		new Document("deptno", 30), 
		new Document ("$mul", new Document("sal", 1.1) ),
		new UpdateOptions());*/

		
		
		//emp.joinLocalAndforeignCollections("colDepts","colEmployes", 
		//"_id", "deptno", "_id", "ENTIER", "10");

	//	emp.groupByOnOneCollectionByJob("colEmployes");

		//
		emp.dropAIndexOfACollection(emp.EmployeCollectionName, "idx_colEmployes_ename");

		// Cr�ation d'un index*
		emp.createEmployeIndexes(
			emp.EmployeCollectionName,
			"idx_"+emp.EmployeCollectionName+"_ename",
			null,
			null,
			null,
			"Mono Colonne",
			true,
			false
		);
		
	
	emp.getAllIndexesOfACollection(emp.EmployeCollectionName);
	

	}catch(Exception e){
		e.printStackTrace();
	}	
   } 
   
   /**
	Constructeur Employe.
	Dans ce constructeur sont effectu�es les activit�s suivantes:
	- Cr�ation d'une instance du client MongoClient
	- Cr�ation d'une BD Mongo appel� RH
	- Cr�ation d'un utilisateur appel� 
	- Chargement du pointeur vers la base RH
   */
   
   Employe(){
		// Creating a Mongo client
		
		 mongoClient = new MongoClient( hostName , port ); 

		// Creating Credentials 
		// RH : Ressources Humaines
		MongoCredential credential; 
		credential = MongoCredential.createCredential(userName, dbName, 
		 passWord.toCharArray()); 
		System.out.println("Connected to the database successfully"); 	  
		System.out.println("Credentials ::"+ credential);  
		// Accessing the database 
		database = mongoClient.getDatabase(dbName); 

   }
   /**
	Cette fonction permet de cr�er une collection
	de nom nomCollection.
	Travail � faire : compl�ter cette m�thode
   */
   public void createCollectionEmploye(String nomCollection){
	//Creating a collection 
	// A compl�ter !!!!!!!!!

	  database.createCollection(nomCollection); 

	  System.out.println("Collection Employes created successfully"); 

   }
   
   
   /**
	Cette fonction permet de supprimer une collection
	de nom nomCollection.
		Travail � faire : compl�ter cette m�thode

   */
   
   public void dropCollectionEmploye(String nomCollection){
		//Drop a collection 
		MongoCollection<Document> colEmployes=null; 
		System.out.println("\n\n\n*********** dans dropCollectionDept *****************");   

		System.out.println("!!!! Collection Emps : "+colEmployes);

		colEmployes=database.getCollection(nomCollection);
		System.out.println("!!!! Collection Emps : "+colEmployes);
		// Visiblement jamais !!!
		if (colEmployes==null)
			System.out.println("Collection inexistante");
		else {
			colEmployes.drop();	
			System.out.println("Collection colEmp removed successfully !!!"); 
	  
		}
		System.out.println("!!!! Collection Employe : "+colEmployes);

   }

   /**
	Cette fonction permet d'ins�rer un employ� dans une collection.
	Travail � faire : compl�ter cette m�thode

   */
   
   public void insertOneEmploye(String nomCollection, Document employe){
		//Drop a collection 
		// A compl�ter !!!!!!!!!!!!

		MongoCollection<Document> colEmployes=database.getCollection(nomCollection);
		colEmployes.insertOne(employe); 
		System.out.println("Document inserted successfully"); 

   }

   /**
	Cette fonction permet de tester la m�thode insertOneEmploye.
	Travail � faire : compl�ter cette m�thode

   */

   public void testInsertOneEmploye(){
	// A compl�ter !!!!!!!!
	Document emp = new Document("_id", 7839666) ;
	emp.append("ename", "KING");
	emp.append("prenoms", Arrays.asList("Leroi", "Mokondji"));
	emp.append("adresse", new Document(
		"numero", 6)
		.append("rue", "Traverse en Escalier")
		.append("ville", "Valbonne")
		.append("codePostal", "06560")
		.append("Pays", "France"));
	emp.append("job", "Pr�sident") ;
	emp.append("mgr", null) ;
	emp.append("hiredate", "17-11-1981") ;
	emp.append("sal", 5000);
	emp.append("comm", null);
	emp.append("deptno", 10);

	this.insertOneEmploye(EmployeCollectionName, emp);

	System.out.println("Document inserted successfully");     
   }

   /**
	Cette fonction permet d'ins�rer plusieurs Employ�s dans une collection
	Travail � faire : compl�ter cette m�thode

	*/

   public void insertManyEmployes(String nomCollection, List<Document> employes){
	// A compl�ter !!!!!!!!!

		MongoCollection<Document> colEmployes=database.getCollection(EmployeCollectionName);
		colEmployes.insertMany(employes); 

		System.out.println("Many Documents inserted successfully");     
   }

   /**
	Cette fonction permet de tester la fonction insertManyEmployes
   */

   public void testInsertManyEmployes(){
		List<Document> employes = Arrays.asList(
		new Document("_id", 7839) 
		.append("ename", "KING")
		.append("prenoms", Arrays.asList("Leroi", "Mokondji"))
		.append("adresse", new Document(
			"numero", 6)
			.append("rue", "Traverse en Escalier")
            .append("ville", "Valbonne")
            .append("codePostal", "06560")
            .append("Pays", "France"))
		.append("job", "Pr�sident") 
		.append("mgr", null) 
		.append("hiredate", "17-11-1981") 
		.append("sal", 5000)
		.append("comm", null)
		.append("deptno", 10),
		
		new Document(
		"_id", 7369) 
		.append("ename", "SMITH")
		.append("prenoms", Arrays.asList("Will", "Williamson"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "Du capitol")
            .append("ville", "Toulouse")
            .append("codePostal", "31000")
            .append("Pays", "France"))
		.append("job", "Clerk") 
		.append("mgr", 7902) 
		.append("hiredate", "17-12-1980") 
		.append("sal", 800)
		.append("comm", null)
		.append("deptno", 20),
		
		new Document(
		"_id", 7499) 
		.append("ename", "ALLEN")
		.append("prenoms", Arrays.asList("Isaora", "Maria"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "Des Mimosas")
            .append("ville", "Toulon")
            .append("codePostal", "83000")
            .append("Pays", "France"))
		.append("job", "Salesman") 
		.append("mgr", 7698) 
		.append("hiredate", "17-02-1981") 
		.append("sal", 1600)
		.append("comm", 300)
		.append("deptno", 30),


		new Document(
		"_id", 7521) 
		.append("ename", "WARD")
		.append("prenoms", Arrays.asList("Julian", "Emery", "Andy"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "Place Garibaldi")
            .append("ville", "Nice")
            .append("codePostal", "06000")
            .append("Pays", "France"))
		.append("job", "Salesman") 
		.append("mgr", 7698) 
		.append("hiredate", "22-FEB-1981") 
		.append("sal", 1250)
		.append("comm", 500)
		.append("deptno", 30),


		new Document(
		"_id", 7566) 
		.append("ename", "JONES")
		.append("prenoms", Arrays.asList("Julian"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "Pertinax")
            .append("ville", "Nice")
            .append("codePostal", "08000")
            .append("Pays", "France"))
		.append("job", "Manager") 
		.append("mgr", 7839) 
		.append("hiredate", "1-04-1981") 
		.append("sal", 2975)
		.append("comm", null)
		.append("deptno", 20),

		
		new Document(
		"_id", 7654) 
		.append("ename", "MARTIN")
		.append("prenoms", Arrays.asList("Le pecheur"))
		.append("adresse", new Document(
			"numero", 10)
			.append("rue", "Canebiere")
            .append("ville", "Marseille")
            .append("codePostal", "13000")
            .append("Pays", "France"))
		.append("job", "Salesman") 
		.append("mgr", 7698) 
		.append("hiredate", "28-11-1981") 
		.append("sal", 1250)
		.append("comm", 1400)
		.append("deptno", 30),


		new Document(
		"_id", 7698) 
		.append("ename", "BLAKE")
		.append("prenoms", Arrays.asList("Lenoir"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "Rond point des Africains")
            .append("ville", "Cogolin")
            .append("codePostal", "83310")
            .append("Pays", "France"))
		.append("job", "Manager") 
		.append("mgr", 7839) 
		.append("hiredate", "1-05-1981") 
		.append("sal", 2850)
		.append("comm", null)
		.append("deptno", 30),


		new Document(
		"_id", 7782) 
		.append("ename", "CLARK")
		.append("prenoms", Arrays.asList("Jonson"))
		.append("adresse", new Document(
			"numero", 27)
			.append("rue", "Des Miracles")
            .append("ville", "Toulouse")
            .append("codePostal", "31000")
            .append("Pays", "France"))
		.append("job", "Manager") 
		.append("mgr", 7839) 
		.append("hiredate", "9-06-1981") 
		.append("sal", 2450)
		.append("comm", null)
		.append("deptno", 10),


		new Document(
		"_id", 7788) 
		.append("ename", "SCOTT")
		.append("prenoms", Arrays.asList("Tiger"))
		.append("adresse", new Document(
			"numero", 75)
			.append("rue", "Jean Jaures")
            .append("ville", "Nice")
            .append("codePostal", "06000")
            .append("Pays", "France"))
		.append("job", "Analyst") 
		.append("mgr", 7566) 
		.append("hiredate", "09-12-1982") 
		.append("sal", 3000)
		.append("comm", null)
		.append("deptno", 20),

		
		new Document(
		"_id", 7844) 
		.append("ename", "TURNER")
		.append("prenoms", Arrays.asList("Allan", "Johnson"))
		.append("adresse", new Document(
			"numero", 11)
			.append("rue", "d'Italie")
            .append("ville", "Nice")
            .append("codePostal", "06000")
            .append("Pays", "France"))
		.append("job", "Salesman") 
		.append("mgr", 7698) 
		.append("hiredate", "8-09-1981") 
		.append("sal", 1500)
		.append("comm", 0)
		.append("deptno", 30),

		
		new Document(
		"_id", 7876) 
		.append("ename", "ADAMS")
		.append("prenoms", Arrays.asList("Le premier"))
		.append("adresse", new Document(
			"numero", 378)
			.append("rue", "Rue du Paradis")
            .append("ville", "Marseille")
            .append("codePostal", "13000")
            .append("Pays", "France"))
		.append("job", "Clerk") 
		.append("mgr", 7788) 
		.append("hiredate", "12-01-1983") 
		.append("sal", 1100)
		.append("comm", null)
		.append("deptno", 20),
		
		new Document(
		"_id", 7900) 
		.append("ename", "JAMES")
		.append("prenoms", Arrays.asList("Bond"))
		.append("adresse", new BasicDBObject(
			"numero", 12)
			.append("rue", "Rue d'Angleterre")
            .append("ville", "Nice")
            .append("codePostal", "06000")
            .append("Pays", "France"))
		.append("job", "Clerk") 
		.append("mgr", 7698) 
		.append("hiredate", "9-12-1981") 
		.append("sal", 950)
		.append("comm", null)
		.append("deptno", 30),
		
		new Document(
		"_id", 7902) 
		.append("ename", "FORD")
		.append("prenoms", Arrays.asList("James"))
		.append("adresse", new BasicDBObject(
			"numero", 11)
			.append("rue", "Des voitures")
            .append("ville", "Toulouse")
            .append("codePostal", "31000")
            .append("Pays", "France"))
		.append("job", "Analyst") 
		.append("mgr", 7566) 
		.append("hiredate", "3-12-1981") 
		.append("sal", 3000)
		.append("comm", null)
		.append("deptno", 20),

		new Document(
		"_id", 7934) 
		.append("ename", "MILLER")
		.append("prenoms", Arrays.asList("Rosa", "Alison"))
		.append("adresse", new BasicDBObject(
			"numero", 11)
			.append("rue", "Place des �toiles")
            .append("ville", "Biarritz")
            .append("codePostal", "64200")
            .append("Pays", "France"))
		.append("job", "Clerk") 
		.append("mgr", 7782) 
		.append("hiredate", "23-01-1982") 
		.append("sal", 1300)
		.append("comm", null)
		.append("deptno", 10)
		
		);  
		this.insertManyEmployes(this.EmployeCollectionName, employes);
   }

   /**
	Cette fonction permet de rechercher un employ� dans une collection
	connaissant son id.
	Travail � faire : compl�ter cette m�thode

   */


  public void getAllEmployes(String nomCollection){

	MongoCollection<Document> colEmployes=database.getCollection(nomCollection);

		
		FindIterable<Document> listEmployes=colEmployes.find();

		// Getting the iterator 
		Iterator it = listEmployes.iterator();
		this.displayIterator(it, "Liste de tous les employ�s");
  }

   public void getEmployeById(String nomCollection, Integer empId){
		// A compl�ter !!!!!!!!!
	
		MongoCollection<Document> colEmployes=database.getCollection(nomCollection);

		Document whereQuery = new Document();

		whereQuery.put("_id", empId);
		FindIterable<Document> listEmployes=colEmployes.find(whereQuery);

		// Getting the iterator 
		Iterator it = listEmployes.iterator();
		while(it.hasNext()) {
				System.out.println(it.next());
		}		
   } 
   
   
    /**
	Cette fonction permet de rechercher des employ�s dans une collection.
	Le parametre whereQuery : permet de passer des conditions de rechercher
	Le parametre projectionFields : permet d'indiquer les champs a afficher
	Le parametre sortFields : permet d'indiquer les champs de tri.
	
	Travail � faire : compl�ter cette m�thode

   */

   public void getEmployes(String nomCollection,Document whereQuery, 
							Document projectionFields,
							Document sortFields){
		// A compl�ter !!!!!!!!!!
		System.out.println("\n\n\n*********** dans Employes *****************");   

		MongoCollection<Document> colEmployes=database.getCollection(nomCollection);

		FindIterable<Document> listEmployes=colEmployes.find(whereQuery).sort(sortFields).projection(projectionFields);

		// Getting the iterator 
		Iterator it = listEmployes.iterator();
		while(it.hasNext()) {
				System.out.println(it.next());
		}	
		System.out.println("OK");
   } 

    /**
	Cette fonction permet de modifier des employ�s dans une collection.
	Le parametre whereQuery : permet de passer des conditions de recherche
	Le parametre updateExpressions : permet d'indiquer les champs a modifier
	Le parametre UpdateOptions : permet d'indiquer les options de mise a jour :
		.upSert : insere si le document n'existe pas
	
	Travail � faire : compl�ter cette m�thode

   */

   public void updateEmployes(String nomCollection, 
							Document whereQuery, 
							Document updateExpressions,
							UpdateOptions updateOptions
							){

		//Drop a collection 
		System.out.println("\n\n\n*********** dans updateDepts *****************");   

		MongoCollection<Document> colEmployes=database.getCollection(nomCollection);
		UpdateResult updateResult = colEmployes.updateMany(whereQuery, updateExpressions);
		
		System.out.println("\nR�sultat update : "
		+"getUpdate id: "+updateResult
		+" getMatchedCount : "+updateResult.getMatchedCount() 
		+" getModifiedCount : "+updateResult.getModifiedCount()
		);

		
   }


    /**
	Cette fonction permet de supprimer des employ�s dans une collection.
	Le parametre filters : permet de passer des conditions de recherche des employ�s a supprimer
	
	Travail � faire : compl�ter cette m�thode

 */
   public void deleteEmployes(String nomCollection, Document filters){
		
		System.out.println("\n\n\n*********** dans deleteEmployes *****************");   

		FindIterable<Document> listEmployes;
		Iterator it;
		MongoCollection<Document> colEmployes=database.getCollection(nomCollection);
		
		listEmployes=colEmployes.find(filters).sort(new Document("_id", 1));
		it = listEmployes.iterator();// Getting the iterator
		this.displayIterator(it, "Dans deleteEmployes: avant suppression");

		colEmployes.deleteMany(filters);
		listEmployes=colEmployes.find(filters).sort(new Document("_id", 1));
		it = listEmployes.iterator();// Getting the iterator
		this.displayIterator(it, "Dans deleteEmployes: Apres suppression");

	} 	
   
   
   /**
	Parcours un it�rateur et affiche les documents qui s'y trouvent
   */
   public void displayIterator(Iterator it, String message){
	System.out.println(" \n #### "+ message + " ################################");
	while(it.hasNext()) {
		System.out.println(it.next());
		
	}		
		
   }

    /**
	Cette fonction permet d'effectuer une jointure entre les collections dept et employe
	Elle permet d'afficher les employ�s par d�partement.
	Le parametre localCollectionName : nom de la 1ere collection a joindre
	Le parametre ForeignCollectionName : nom de la 2eme collection a joindre
	Le parametre localColJoinFieldName : champ de jointure de la 1 ere collection
	Le parametre foreigColJoinFieldName :  champ de jointure de la 2 eme collection
	Le parametre filterFieldName : Champ de restriction
	Le parametre filterFieldType :  type du champ. Entier pour l'instant
	Le parametre filterFieldValue : Valeur du champ de restriction
	
	Travail � faire : compl�ter cette m�thode

	
   */

	   
   public void joinLocalAndforeignCollections(
	String localCollectionName, 
	String foreignCollectionName, 
	String localColJoinFieldName,
	String foreigColJoinFieldName,
	String filterFieldName,
	String filterFieldType,
	String filterFieldValue){
	// A compl�ter !!!!!!!!!!!!
	
	MongoCollection<Document> colDepts = database.getCollection(localCollectionName);
	
	AggregateIterable<Document> result = colDepts.aggregate(

		Arrays.asList(
			Aggregates.match(Filters.eq(filterFieldName,Integer.parseInt(filterFieldValue))),
			Aggregates.lookup(foreignCollectionName, localColJoinFieldName,foreigColJoinFieldName, "emp_dept")
		)

	) ;

	Iterator iterator = result.iterator();
	this.displayIterator(iterator, "Jointure Col et Emp");

	}

	

   /**
	Cette fonction permet d'effectuer des op�rations de groupes
	dans la collection Employe sur le champ job.
	Elle renvoie :
		- La masse salariale par job
		- La moyenne des salaires par job
		- Le nombre de salaire par job.
	
	
	Travail � faire : compl�ter cette m�thode

   */

   public void groupByOnOneCollectionByJob(
	String localCollectionName
	){
		
		// A compl�ter !!!!!!!!
		MongoCollection<Document> colEmployes = database.getCollection(localCollectionName);
	
		AggregateIterable<Document> result = colEmployes.aggregate(
	
			Arrays.asList(
				Aggregates.group("$job", 
								 Accumulators.sum("masse_salariale", "$sal"),
								 Accumulators.avg("moyenne_salaire", "$sal"),
								 Accumulators.sum("nombre_salari�", 1))			)
	
		) ;
	
		Iterator iterator = result.iterator();
		this.displayIterator(iterator, "Group by job");
	}
	

    /**
	Cette fonction permet de cr�er un index dans la collection.
		- Le parametre localCollectionName : Nom de la collection
		- Le parametre indexName : Nom de l'indexe
		- Le parametre indexFieldName1 : Nom du 1er champ d'index
		- Le parametre indexFieldName2 : Nom du 2eme champ d'index s'il y a
		- Le parametre indexFieldName3 : Nom du 3eme champ d'index s'il y a
		- Le parametre indexType : Type d'index
		- Le parametre isAscendingIndex : true si ascendant, false sinon
		- Le parametre indexUnique : true si index unique false sinon
		
		Travail � faire : compl�ter cette m�thode

	
   */
	public void createEmployeIndexes(
		String localCollectionName,
		String indexName,
		String indexFieldName1,
		String indexFieldName2,
		String indexFieldName3,
		String indexType,
		boolean isAscendingIndex,
		boolean indexUnique
	){
		
		
		System.out.println("\n\n\n*********** dans createEmployeIndexes *****************");   
		// A compl�ter !!!!!!!!!!!!!!!!!!

		MongoCollection<Document> colEmployes = database.getCollection(localCollectionName);
	
		IndexOptions indexOptions;

		if(indexUnique == true){
			 indexOptions = new IndexOptions().unique(true);
		}else{
			indexOptions = new IndexOptions().unique(false);
		}
		
		//Nom de l'index
		indexOptions = indexOptions.name(indexName);

		
		if(indexFieldName1 != null && indexFieldName1 ==null && indexFieldName3 == null){


			if(isAscendingIndex == true){
				colEmployes.createIndex(
					Indexes.ascending(indexFieldName1),indexOptions);
			}else{
				colEmployes.createIndex(
					Indexes.descending(indexFieldName1),
					indexOptions);
			}

		}


		if(indexFieldName1 != null && indexFieldName2 != null && indexFieldName3 ==null){

			//indexOptions.setName(indexName);

			if(isAscendingIndex == true){
				colEmployes.createIndex(
					Indexes.compoundIndex(
					Indexes.ascending(indexFieldName1),
					Indexes.ascending(indexFieldName2)),
					indexOptions
					);
			}else{
				colEmployes.createIndex(
					Indexes.compoundIndex(
					Indexes.descending(indexFieldName1),
					Indexes.descending(indexFieldName2)),
					indexOptions
					);			
				}
		}

		if(indexFieldName1 != null && indexFieldName2 != null && indexFieldName3 !=null){

			//indexOptions.setName(indexName);

			if(isAscendingIndex == true){
				colEmployes.createIndex(
					Indexes.compoundIndex(
					Indexes.ascending(indexFieldName1),
					Indexes.ascending(indexFieldName2),
					Indexes.ascending(indexFieldName3)),
					indexOptions
					);
			}else{
				colEmployes.createIndex(
					Indexes.compoundIndex(
					Indexes.descending(indexFieldName1),
					Indexes.descending(indexFieldName2),
					Indexes.descending(indexFieldName3)),
					indexOptions
					);			
				}
		}
	
	}

    /**
	Cette fonction affiche les informations sur tous les indexes d'une collection.
	
	Travail � faire : compl�ter cette m�thode

   */
	public void getAllIndexesOfACollection(
		String localCollectionName
	){
		
		
		System.out.println("\n\n\n*********** dans getAllIndexesOfACollection *****************");   
		MongoCollection<Document> colEmployes = database.getCollection(localCollectionName);
		this.displayIterator(colEmployes.listIndexes().iterator(), "Liste des index des indexs");

	}


   /**
	Cette fonction supprime un index dans une collection connaissant son nom.
	
	Travail � faire : compl�ter cette m�thode

   */
	public void dropAIndexOfACollection(
		String localCollectionName,
		String indexName
	){
		
		
		System.out.println("\n\n\n*********** dans dropAIndexOfACollection *****************");   
		MongoCollection<Document> colEmployes = database.getCollection(localCollectionName);
		
		try{

			colEmployes.dropIndex(indexName);
			System.out.println("\n\n\n Index : " +indexName+" Supprim� avec succ�s");   



		}catch(Exception e){

			System.out.println("\n\n\nAucun index trouv�");   

		}


	}

}
